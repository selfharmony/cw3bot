from telethon import TelegramClient, events
import asyncio, re, socks, sys, multiprocessing, time
from os import environ
from collections import defaultdict

api_id = 309116
api_hash = '3e0ef95df207dde3ab60b722d198502a'
bot_code = '663843066:AAEwTZHAK5YISzzAKQUWOlO-d44KzHG7Z14'

# A list of dates of reactions we've sent, so we can keep track of floods
recent_reacts = defaultdict(list)

host = '165.227.214.55'  # a valid host
port = 2018  # a valid port

proxy = (socks.SOCKS5, host, port)

# TG_API_ID and TG_API_HASH *must* exist or this won't run!
session_name = environ.get('TG_SESSION', 'session')
client = TelegramClient(
    session_name, api_id, api_hash,
    proxy=None
)


@client.on(events.NewMessage)
@asyncio.coroutine
def my_handler(event: events.NewMessage.Event):
    id = event.message.from_id
    yield from client.send_message(id, "works!!")
    time.sleep(0.1)


@asyncio.coroutine
def run_client():
    yield from client.connect()
    yield from client.sign_in(bot_token=bot_code)
    yield from client.start()
    print("BOT STARTED")
    yield from client.run_until_disconnected()


def main():
    tasks = [
        run_client()
    ]
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*tasks))


if __name__ == '__main__':
    main()
