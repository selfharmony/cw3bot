from __future__ import print_function
import sys
from datetime import datetime
from subprocess import call
import urllib.request as urllib
import re
from subprocess import call
import os, ssl
import asyncio

dl_url = "https://api.bitbucket.org/1.0/repositories/selfharmony/cw3bot/raw/HEAD/bot_test.py"


def compare_versions(vA, vB):
    if vA == vB:
        return 0
    elif vA < vB:
        return -1
    else:
        return 1


def update(force_update=False):
    if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
            getattr(ssl, '_create_unverified_context', None)):
        ssl._create_default_https_context = ssl._create_unverified_context
    # dl the first 256 bytes and parse it for version number
    try:
        http_stream = urllib.urlopen(dl_url)
        update_file = http_stream.read(256)
        http_stream.close()
    except IOError as err:
        print("Unable to retrieve version data")
        print("Error %s: %s" % (err.errno, err.strerror))
        return

    match_regex = re.search(r'__version__ *= *"(\S+)"', str(update_file))
    if not match_regex:
        print("No version info could be found")
        return
    update_version = match_regex.group(1)

    if not update_version:
        print("Unable to parse version data")
        return

    if force_update:
        print("Forcing update, downloading version %s..." \
              % update_version)
    else:
        from bot_test import __version__
        cmp_result = compare_versions(__version__, update_version)
        if cmp_result < 0:
            print("Newer version %s available, downloading..." % update_version)
        elif cmp_result > 0:
            print("Local version %s newer then available %s, not updating." \
                  % (__version__, update_version))
            return
        else:
            print("You already have the latest version.")
            return

    # dl, backup, and save the updated script
    app_path = os.path.realpath(sys.argv[0])

    if not os.access(app_path, os.W_OK):
        print("Cannot update -- unable to write to %s" % app_path)

    dl_path = app_path + ".new"
    backup_path = app_path + ".old"
    try:
        dl_file = urllib.urlretrieve(dl_url, dl_path)
    except IOError as err:
        print("Download failed")
        print("Error %s: %s" % (err.errno, err.strerror))
        return

    try:
        os.rename(app_path, backup_path)
    except OSError as err:
        print("Unable to rename %s to %s: (%d) %s" \
              % (app_path, backup_path, err.errno, err.strerror))
        return

    try:
        os.rename(dl_path, app_path)
    except OSError as err:
        print("Unable to rename %s to %s: (%d) %s" \
              % (dl_path, app_path, err.errno, err.strerror))
        return

    try:
        import shutil
        shutil.copymode(backup_path, app_path)
    except:
        os.chmod(app_path, 0o755)

    print("New version installed as %s" % app_path)
    print("(previous version backed up to %s)" % (backup_path))
    print("exiting")
    sys.exit(-1)


# @asyncio.coroutine
def get_version_info():
    if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
            getattr(ssl, '_create_unverified_context', None)):
        ssl._create_default_https_context = ssl._create_unverified_context
    # dl the first 256 bytes and parse it for version number
    try:
        http_stream = urllib.urlopen(dl_url)
        update_file = http_stream.read(256)
        http_stream.close()
    except IOError as err:
        return

    match_regex = re.search(r'__version__ *= *"(\S+)"', str(update_file))
    if not match_regex:
        return
    update_version = match_regex.group(1)

    if not update_version:
        return

    else:
        from bot_test import __version__
        cmp_result = compare_versions(__version__, update_version)
        if cmp_result < 0:
            print(
                "\n============\nВышла новая версия бота %s, обновите...\n============\n" % update_version)
            return
        else:
            print(
                "\n============\nВаша версия бота %s, последняя \n============\n" % update_version)
            return


# dependencies

def installPip(log=print):
    log("Installing pip, the standard Python Package Manager, first")
    from os import remove
    from urllib.request import urlretrieve
    urlretrieve("https://bootstrap.pypa.io/get-pip.py", "get-pip.py")
    call(["python", "get-pip.py"])

    # Clean up now...
    remove("get-pip.py")


def get_pip(log=print):
    # fix if no certificate
    import os, ssl
    if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
            getattr(ssl, '_create_unverified_context', None)):
        ssl._create_default_https_context = ssl._create_unverified_context

    """
    Pip is the standard package manager for Python.
    This returns the path to the pip executable, installing it if necessary.
    """
    from os.path import isfile, join
    from sys import prefix
    # Generate the path to where pip is or will be installed... this has been
    # tested and works on Windows, but will likely need tweaking for other OS's.
    # On OS X, I seem to have pip at /usr/local/bin/pip?
    if os.name == 'nt':
        pipPath = join(prefix, 'Scripts', 'pip.exe')
    else:
        from subprocess import Popen, PIPE
        finder = Popen(['which', 'pip'], stdout=PIPE, stderr=PIPE)
        pipPath = finder.communicate()[0].strip()

    # Check if pip is installed, and install it if it isn't.
    if not isfile(pipPath):
        installPip(log)
        if not isfile(pipPath):
            raise ("Failed to find or install pip!")
    return pipPath


def install_if_needed(moduleName, nameOnPip=None, notes="", log=print):
    """ Installs a Python library using pip, if it isn't already installed. """
    from pkgutil import iter_modules

    # Check if the module is installed
    if moduleName not in [tuple_[1] for tuple_ in iter_modules()]:
        log("Installing " + moduleName + notes + " Library for Python")
        call([get_pip(log), "install", nameOnPip if nameOnPip else moduleName])


def log(message):
    print(datetime.now().strftime("%a %b %d %H:%M:%S") + " - " + str(message))


def install_dependencies():
    install_if_needed("socks", "PySocks", log=log)
    install_if_needed("asyncio", "asyncio", log=log)
    install_if_needed("telethon", "telethon", log=log)
