# from bot_test import get_quantity, buy_resources
#
# msg = "/a_06 Charcoal x 11  \n/a_k18 Clarity Bracers part x 1 \n/a_09 Cloth x 39 \n/a_05 Coal x " \
#       "49 \n/a_23 Coke x 5 \n/a_08 Iron ore x 3 \n/a_20 Leather x 4 \n/a_13 Magic stone x 3 " \
#       "\n/a_03 Pelt x 6 \n/a_07 Powder x 118 \n/a_25 Silver alloy x 1 \n/a_19 Steel x 1 \n/a_02 " \
#       "Stick x 383 \n/a_01 Thread x 73 \n/a_14 Wooden shaft x 2 "
#
# msg2 = "/craft_20 Leather"
#
# print(get_quantity("Leather", msg)<4)
import string


class CallingDict(dict):
    def __getitem__(self, item):
        it = super(CallingDict, self).__getitem__(item)
        if callable(it):
            return it(self)
        else:
            return it


# S = SurrogateDict(AdditionSurrogateDictEntry)
# d = S.resolve({'user': 'gnucom',
#                'home': '/home/' + S['user']})

xs = CallingDict({
    'user': 'gnucom',
    'home': '/home/{user}',
    'bin': '{home}/bin'
})


def get_total_price(items: CallingDict):
    result = 0
    for item in items:
        if not item["av_price"] is None:
            result += item["av_price"] * item["amount"]
    return result


dictionary1 = {
    "HunterArmor": CallingDict({
        "must_craft": False,
        "to_craft": 1,
        "sum": 0,
        "mat_cost": lambda c: get_total_price(c["materials"]),
        "craft": None,
        "materials": [
            CallingDict({"name": "Coke", "amount": 9, "av_price": 14, "can_buy": True,
                         "code": lambda m: "wtb_23" + "_" + str(m["amount"])}),
            CallingDict({"name": "Leather", "amount": 24, "av_price": 21, "can_buy": True,
                         "code": lambda m: "wtb_20" + "_" + str(m["amount"])}),
            CallingDict({"name": "Rope", "amount": 12, "av_price": 14, "can_buy": True,
                         "code": lambda m: "wtb_31" + "_" + str(m["amount"])}),
            CallingDict({"name": "Sapphire", "amount": 3, "av_price": 29, "can_buy": True,
                         "code": lambda m: "wtb_15" + "_" + str(m["amount"])}),
            CallingDict({"name": "Solvent", "amount": 5, "av_price": 2, "can_buy": True,
                         "code": lambda m: "wtb_16" + "_" + str(m["amount"])}),
            CallingDict({"name": "Silver mold", "amount": 3, "av_price": None, "can_buy": False,
                         "code": None}),
            CallingDict(
                {"name": "Hunter Armor part", "amount": 3, "av_price": None, "can_buy": False,
                 "code": None}),
            CallingDict(
                {"name": "Hunter Armor recipe", "amount": 1, "av_price": None, "can_buy": False,
                 "code": None})
        ]}),
    "Silver mold": {
        "must_craft": False,
        "to_craft": 1,
        "sum": 0,
        "mat_cost": lambda c: get_total_price(c["materials"]),
        "craft": None,
        "materials": [
            CallingDict({"name": "Coke", "amount": 2, "av_price": 14, "can_buy": True,
                         "code": lambda m: "wtb_23" + "_" + str(m["amount"])}),
            CallingDict({"name": "String", "amount": 2, "av_price": 21, "can_buy": True,
                         "code": lambda m: "wtb_20" + "_" + str(m["amount"])}),
            CallingDict({"name": "Silver Ore", "amount": 2, "av_price": 21, "can_buy": True,
                         "code": lambda m: "wtb_10" + "_" + str(m["amount"])}),
        ]}
}

print(dictionary1["HunterArmor"]["mat_cost"])
