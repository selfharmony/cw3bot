import re
import ast


def recipes_to_get():
    file_object = open("./src/tests/craftbook", "r")
    c = [x.strip() for x in file_object.readlines()]
    return c


def get_recipe(msg):
    def find_materials():

        def replace_a_split(item):
            if '/a_' in item:
                item = item.replace('/a_', '/view_')
                split_amount = item.split(') x ')
                split_name = item.split(' (')
                name = split_name[0]
                code = split_name[1].split(')')[0]
                amount = split_amount[1]
                return [name, code, amount]
            elif '/t_' in item:
                global recipe_name
                name_array = item.split(' (')
                recipe_name = name_array[0]
            elif 'Требует 💧:' in item:
                global mana
                mana_array = item.split(': ')
                mana = mana_array[1]
            else:
                return None

        items = [x.strip() for x in msg.splitlines()]
        result = list(map(replace_a_split, items))
        materials = list(filter((lambda x: x is not None), result))
        return {
            'name': recipe_name,
            'mana': mana,
            'materials': materials
        }

    return find_materials()


def append_recipes(msg):
    recipe = str(get_recipe(msg)) + '\n'
    f = open('./src/tests/recipes_backup', 'a')
    f.write(recipe)


def process_reply(msg):
    append_recipes(msg)


# res = {}
#
# file_object = open("raw_recipes", "r")
#
# c = [x.strip() for x in file_object.readlines()]
#
# for i in c:
#     print_code(i)
