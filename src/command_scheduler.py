import time
import game_data as cc

threads = {}
delay = 5

def minutes_sleeper_loop(command, minutes: int):
    time.sleep(delay)
    while 1:
        command()
        time.sleep(60 * minutes)


async def hours_sleeper_loop(command, hours: int):
    time.sleep(delay)
    while 1:
        await command()
        time.sleep(hours)
        # time.sleep(3600 * hours)


async def schedule_forest():
    while True:
        await hours_sleeper_loop(cc.go_forest, 1)
    # t = threading.Thread(target=hours_sleeper_loop, args=(cc.go_forest, 1))
    # threads["forest"] = t
    # t.start()
