from telethon import TelegramClient

from src.realbot.interaction.client_creator import ProxyProvider, TgClientProvider
from src.realbot.interaction.command import Commander
from src.realbot.interaction.input import InputProcessor
from src.realbot.interaction.task_control import TaskController
from src.realbot.interaction.interact import Interactor, Replier
import asyncio

loop = asyncio.get_event_loop()

proxies: ProxyProvider = ProxyProvider()
client: TelegramClient = TgClientProvider(proxy=proxies.get(1)).client()
tasker: TaskController = TaskController()
commander: Commander = Commander(client, tasker)
inputter: InputProcessor = InputProcessor(commander)

interactor = Interactor(client, commander, inputter)


def main():
    tasks = [
        interactor.run(),
    ]
    loop.run_until_complete(asyncio.gather(*tasks))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("exited")
    except RuntimeError:
        print('interrupted!')
    finally:
        loop.close()
