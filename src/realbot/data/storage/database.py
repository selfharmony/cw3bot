from pymongo import MongoClient


class BaseCollection:
    def __init__(self, db, name):
        self._col = db[name]

    def get_all(self):
        # myquery = {"name": {"$regex": "."}}
        result = []
        cursor = self._col.find()
        for doc in cursor:
            result.append(doc)
        return result

    def get_many(self, query):
        result = []
        cursor = self._col.find(query)
        for doc in cursor:
            result.append(doc)
        return result

    def get_item(self, name):
        return self._col.find_one({"name": name})

    def put_single(self, name, amount):
        self._col.find_one_and_replace({"name": name}, {"name": name, "amount": amount},
                                       upsert=True)

    def replace_by_name(self, name, item):
        self._col.find_one_and_replace({"name": name}, item, upsert=True)

    def add_item(self, item):
        self._col.insert_one(item)

    def remove_item(self, name):
        self._col.delete_one({"name": name})

    def clear(self):
        self._col.delete_many({})

    def drop(self):
        self._col.drop()


class Money(BaseCollection):
    def __init__(self, _db):
        super().__init__(_db, "money")

    def get_gold(self):
        return self.get_item("gold")

    def put_gold(self, amount: int):
        self.put_single("gold", amount)

    def get_pouch(self):
        return self.get_item("pouch")

    def put_pouch(self, amount: int):
        self.put_single("pouch", amount)


class GameData:
    def __init__(self):
        self.__client = MongoClient()
        self._db = self.__client["gamedata"]
        self.money = Money(self._db)
        self.to_buy = BaseCollection(self._db, "to-buy")
        self.hidden = BaseCollection(self._db, "hidden")
        self.recipes = BaseCollection(self._db, "recipes")

    def print_collections(self):
        print(self._db.list_collection_names())
    #
    # def test(self):
    #     self.__db["money"].find_one_and_replace()


gd = GameData()
print(gd.to_buy.get_all())
# gd.money.put_gold(111)
