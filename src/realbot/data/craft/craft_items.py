import time

from src.realbot.data.storage.database import BaseCollection, GameData
from src.realbot.interaction.command import Commander


class Trader:
    def __init__(self, commander: Commander = None):  # Todo: implement commander
        self._commander = commander

    def buy_resources(self, materials: list):
        for m in materials:
            print('buying: ' + m["code"] + ", ", m["amount"])  # Todo: implement message
            time.sleep(3)


class Blocker:
    def __init__(self, current_block: list):
        self._block = current_block

    def __add_block(self, command):
        self._block.append(command)

    def __execute_first(self):
        self._block[0]()
        self._block.remove(self._block[0])

    def execute(self, command):
        if len(self._block) > 0:
            self.__add_block(command)
            time.sleep(5)
            self.__execute_first()
        else:
            command()


class HeroSyncer:
    def __init__(self, commander: Commander = None):  # Todo: implement commander
        self._commander = commander

    # Todo sycnc Stock and check crom synced data !!!
    def get_stock_materials(self, materials: list):
        result = []
        for m in materials:
            we_have = {'code': m['code'], 'amount': '2'}
            time.sleep(3)
            print('we have: ' + str(we_have))  # Todo: implement message and REGEX
            result.append(we_have)
        return result


class Crafter:
    def __init__(self, db: BaseCollection, trader: Trader, hero_syncer: HeroSyncer,
                 commander: Commander = None):  # Todo: implement commander
        self._db = db
        self._trader = trader
        self._commander = commander
        self._hero_syncer = hero_syncer

    def crafted_before(self, code: int):
        return False  # Todo: implement message

    def craft_by_code(self, code: str):
        print('/craft_' + code)  # Todo: implement request message

    def manual_craft(self, recipe: dict):
        def add_materials(recipe):
            for m in recipe['materials']:
                print('/a_' + m['code'] + '_' + m['amount'])  # Todo: implement message
                time.sleep(3)

        add_materials(recipe)
        print("CRAFT!")  # Todo: implement message
        time.sleep(3)

    def get_remaining_materials(self, need: list, stock: list):
        to_buy = []
        need_gold = 0
        have_gold = self._hero_syncer.get_gold()
        for m in need:
            need_amount = int(m['amount'])
            if m['name'] != 'Gold':
                have_amount = int(stock[need.index(m)]['amount'])
                if need_amount > have_amount:
                    difference = need_amount - have_amount
                    to_buy.append({'code': m['code'], 'amount': difference})
            else:
                have_amount = self._hero_syncer.get_gold()

        return to_buy

    def craft(self):
        recipes = self._db.get_all()
        for r in recipes:
            if r['to_craft'] > 0:
                if r['must_buy']:
                    stock_materials = self._hero_syncer.get_stock_materials(r['materials'])
                    to_buy = self.get_remaining_materials(r['materials'], stock_materials)
                    if len(to_buy) > 0:
                        self._trader.buy_resources(to_buy)
                if self.crafted_before(r['code']):
                    self.craft_by_code(r['code'])
                else:
                    self.manual_craft(r)


db = GameData().recipes
crafter = Crafter(db, Trader(), HeroSyncer())
crafter.craft()
