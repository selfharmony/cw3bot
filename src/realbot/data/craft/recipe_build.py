import collections
import re
import ast

from src.realbot.data.storage.database import GameData


def recipes_to_get():
    file_object = open("./recipes", "r")
    c = [x.strip() for x in file_object.readlines()]
    return c


def item_codes_to_get():
    file_object = open("./item_codes", "r")
    c = [x.strip() for x in file_object.readlines()]
    return c


def get_dicts_list(lst):
    dicts_list = []
    for i in lst:
        dicts_list.append(ast.literal_eval(i))
    return dicts_list


def get_recipes_list(lst):
    codes: list = item_codes_to_get()
    rec_list = []
    for recipe in lst:
        materials = []
        for m in recipe['materials']:
            materials.append({
                "name": m[0],
                "code": m[1],
                "amount": m[2]
            })
        result_recipe = {
            "name": recipe["name"],
            "mana": recipe["mana"],
            "code": codes[lst.index(recipe)],
            "to_craft": 0,
            "must_buy": True,
            "materials": materials
        }
        rec_list.append(result_recipe)
    return rec_list


def print_recipes_list(recipes: list, is_sublist=False):
    for recipe in recipes:
        print(recipe)


recipes_db = GameData().recipes

lst = recipes_to_get()
dicts = get_dicts_list(lst)
recipes = get_recipes_list(dicts)
# recipes_db.clear()
# for r in recipes:
#     recipes_db.add_item(r)

print(recipes_db.get_all())

