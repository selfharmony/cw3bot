import asyncio


class TaskController:
    __game_tasks = []

    def task_info(self):
        sb = "["
        for t in self.__game_tasks:
            sb += t.name + ", "
        sb += "]"
        return sb

    def show_tasks(self):
        print(self.task_info())

    def register_task(self, task: asyncio.Task, name):
        if task is not None:
            self.__game_tasks.append(task)
            task.name = name

    def register_current_method(self, name):
        if asyncio.Task.current_task() is not None:
            task = asyncio.Task.current_task()
            self.__game_tasks.append(task)
            task.name = name

    def stop_task(self, name):
        for task in self.__game_tasks:
            if task.name == name:
                task.cancel()
                self.__game_tasks.remove(task)
                print(name + " stopped by TaskController")

    def task_running(self, name):
        for task in self.__game_tasks:
            if task.name == name:
                if not task.done():
                    return True
        return False

    def close_all(self):
        for t in self.__game_tasks:
            t.cancel()
            self.__game_tasks.remove(t)
            print(self.task_info() + " stopped by TaskController")

    def task(self, command, *args):
        self.register_task(asyncio.ensure_future(command(*args)), command.__name__)
        return command

    def unregistered_task(self, command, *args):
        asyncio.ensure_future(command(*args))
