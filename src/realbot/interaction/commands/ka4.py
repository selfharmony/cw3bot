import asyncio
from concurrent.futures import CancelledError

from src.realbot.interaction.command import Commands


@asyncio.coroutine
def proceed(commands: Commands):
    try:
        yield from commands.message("123")
        yield from asyncio.sleep(4)
    except CancelledError:
        print('ka4 cancelled')
    except RuntimeError:
        print('ka4 cancelled')