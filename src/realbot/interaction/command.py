import asyncio

from telethon import TelegramClient

# import src.realbot.data.game_constants as constants
from src.realbot.interaction.task_control import TaskController


class Commands:
    import src.realbot.interaction.commands as commands
    __client: TelegramClient

    def __init__(self, client: TelegramClient):
        self.__client = client

    @asyncio.coroutine
    def quest(self):
        yield from asyncio.sleep(1)
        yield from print("quest")

    @asyncio.coroutine
    def ka4(self):
        pass
        # yield from self.commands.ka4.proceed(self)

    @asyncio.coroutine
    def korovan(self):
        yield from asyncio.sleep(1)
        yield from print("korovan")

    @asyncio.coroutine
    def message(self, msg):
        yield from asyncio.sleep(1)
        yield from self.__client.send_message(constants.charwars_username, msg)

    @asyncio.coroutine
    def delete_message(self, from_id, message_id):
        yield from asyncio.sleep(1)
        yield from self.__client.delete_messages(from_id, message_id)


class Commander:
    __client: TelegramClient
    __tasker: TaskController
    __commands: Commands

    def __init__(self, client: TelegramClient, tasker: TaskController):
        self.__client = client
        self.__tasker = tasker
        self.__commands = Commands(client)

    def get_tasker(self):
        return self.__tasker

    def get_client(self):
        return self.__client

    def stop(self, command):
        self.__tasker.stop_task(command.__name__)

    def message(self, msg):
        self.__tasker.unregistered_task(self.__commands.message, msg)

    def delete_message(self, from_id, message_id):
        self.__tasker.unregistered_task(self.__commands.delete_message, from_id, message_id)

    # GAME COMMANDS
    def quest(self):
        self.__tasker.task(self.__commands.quest)

    def ka4(self):
        self.__tasker.task(self.__commands.ka4)

    def korovan(self):
        self.__tasker.task(self.__commands.korovan)

    # GAME REPLIES
    def intercept_korovan(self):
        self.message(constants.go)

