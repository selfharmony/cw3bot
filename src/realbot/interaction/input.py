import asyncio
import sys
import src.realbot.data.game_constants as constants

from src.realbot.interaction.command import Commander


class InputProcessor:
    __loop = asyncio.get_event_loop()
    __commander = None
    __client = None
    __tasker = None

    def __init__(self, commander: Commander):
        self.__commander = commander
        self.__client = commander.get_client()
        self.__tasker = commander.get_tasker()

    @asyncio.coroutine
    def async_input(self, prompt):
        print(prompt, end='', flush=True)
        return (yield from self.__loop.run_in_executor(None, sys.stdin.readline)).rstrip()

    @asyncio.coroutine
    def run_input_loop(self):
        print('инициализация ввода команд\n')
        print('вводите команды')

        while True:
            msg = yield from self.async_input('')
            if msg == '':
                pass
            elif msg == 'q':
                self.__tasker.close_all()
                sys.exit(-1)
            elif msg == 'f':
                self.__commander.quest()
            elif msg == 'ff':
                self.__commander.quest()
            elif msg == 'k':
                self.__commander.korovan()
            elif msg == 'ka4':
                self.__commander.ka4()
            elif msg == 'logout':
                self.__tasker.close_all()
                yield from self.__client.log_out()
                print("logged out")
            else:
                self.__commander.message(msg)

    def run(self):
        self.__tasker.task(self.run_input_loop)
