from typing import Optional

from telethon import TelegramClient


class Proxy:
    import socks

    __host = None
    __port = None
    __login = None
    __password = None
    __protocol = socks.SOCKS5

    def __init__(self, host=None, port=None, login=None, password=None, protocol=socks.SOCKS5):
        self.__host = host
        self.__port = port
        self.__login = login
        self.__password = password
        self.__protocol = protocol

    def get(self):
        proxy = (self.__protocol, self.__host, self.__port, None, self.__login, self.__password)
        return proxy


class ProxyProvider:
    __proxy_list = [
        Proxy(host='212.22.77.47', port=1080, login="friends", password="BondCompactPremiumVipLuxeryDeluxe"),
        Proxy(host='sreju5h4.spry.fail', port=1080, login="telegram", password="telegram"),
    ]

    def get(self, number: int):
        proxy_list = self.__proxy_list
        if number in range(0, len(proxy_list)):
            return proxy_list[number].get()
        else:
            return None

    def get_active(self):  # TODO: auto search for active proxy
        pass


class TgClientProvider:
    from os import environ

    __api_id = 309116
    __api_hash = '3e0ef95df207dde3ab60b722d198502a'
    __session_name = environ.get('TG_SESSION', 'session')
    __client = None

    def __init__(self, proxy: Optional[tuple]):
        self.__client = TelegramClient(
            self.__session_name,
            self.__api_id,
            self.__api_hash,
            proxy=proxy)

    def client(self):
        return self.__client
