import asyncio
import time

from telethon import TelegramClient, events

import src.realbot.data.game_constants as constants
from src.realbot.interaction.command import Commander
from src.realbot.interaction.input import InputProcessor
from src.realbot.interaction.task_control import TaskController


class Replier:
    __commander: Commander

    def __init__(self, commander: Commander):
        self.__commander = commander

    def reply(self, event):
        msg = event.message.message
        f_id = event.message.from_id
        print("\n", time.asctime(), '-\n', msg, "\n")


class Interactor:
    __client: TelegramClient
    __commander: Commander
    __replier: Replier
    __input: InputProcessor
    __tasker: TaskController

    def __init__(self, client: TelegramClient, commander: Commander, input=InputProcessor):
        self.__client = client
        self.__commander = commander
        self.__input = input
        self.__replier = Replier(commander)
        self.__tasker = commander.get_tasker()
        self.listen_event(client)

    def listen_event(self, client):
        @client.on(events.NewMessage)
        @asyncio.coroutine
        def handle_message(event: events.NewMessage.Event):
            if event.message.from_id == constants.charwars_id:
                yield from asyncio.sleep(1.0)
                yield from self.__replier.reply(event)
                yield from asyncio.sleep(2.0)

    @asyncio.coroutine
    def run(self):
        yield from self.__client.start()
        print(constants.info)
        self.__input.run()
        yield from self.__client.run_until_disconnected()
