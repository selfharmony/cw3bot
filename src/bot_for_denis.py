__version__ = "13"

import asyncio, re, socks, sys, multiprocessing, time
from asyncio import CancelledError

from collections import defaultdict
from datetime import datetime
from os import environ
from telethon import TelegramClient, events

from distutils.core import setup
import py2exe
setup(console=['hello.py'])


api_id = 309116
api_hash = '3e0ef95df207dde3ab60b722d198502a'

REACTS = {'emacs': 'Needs more vim',
          'chrome': 'Needs more Firefox'}

# A list of dates of reactions we've sent, so we can keep track of floods
recent_reacts = defaultdict(list)

host = '185.12.177.209'  # a valid host
port = 80  # a valid port

proxy = (socks.HTTP, host, port)

# TG_API_ID and TG_API_HASH *must* exist or this won't run!
session_name = environ.get('TG_SESSION', 'session')
client = TelegramClient(
    session_name, api_id, api_hash,
    proxy=proxy
)

# app constants
loop = asyncio.get_event_loop()
exit_flg = False
game_tasks = []

# game constants
charwars_username = '@ChatWarsBot'
charwars_id = 265204902
notify_id = '@SuperBoltunBot'
k_hour = 4
activities = {'isForest': False, 'isKorovan': False, 'isFastForest': False, 'isKa4': False}
stamina = {'now': 5, 'total': 5}
cycles = {'forest': 100}

local_money = 0
go_korovan_m = "🗡ГРАБИТЬ КОРОВАНЫ"
get_time_m = "/time"
# go_forest_m = "🌲Лес"

resources = {
    "pouch":
        {"must_craft": True,
         "to_craft": 99,
         "sum": 120,
         "mat_cost": 56,
         "craft": "/craft_100",
         "materials": [
             {"name": "Thread", "code": "/wtb_01_16", "amount": 16, "av_price": 1,
              "must_buy": False},
             {"name": "Leather", "code": "/wtb_20_2", "amount": 2, "av_price": 21,
              "must_buy": False}
         ]}
}

# ///////////////////////main////////////////////////////


def register_game_task(name):
    task = asyncio.Task.current_task()
    game_tasks.append(task)
    task.name = name


def stop_activity(name):
    for task in game_tasks:
        if task.name == name:
            task.cancel()


def activity_running(name):
    for task in game_tasks:
        if task.name == name:
            if not task.done():
                return True
    return False


def find_between(string, start, end):
    result = re.search(start + '(\d*)' + end, string)
    return result.group(1)


# флаг накопления необходимой суммы для крафта того или иного ресурса
@asyncio.coroutine
def craft_pouch(msg, f_id):
    if (resources["pouch"]["must_craft"]) & (resources["pouch"]["to_craft"] > 0):
        if int(find_between(msg, '💰', ' 👝')) >= resources["pouch"]["sum"]:
            time.sleep(1)
            yield from client.send_message(f_id, resources["pouch"]["craft"])
            time.sleep(1)
            yield from sync_hero_info()
            resources["pouch"]["to_craft"] -= 1
            # вызов /stock когда [недостаточно ресурсов]
        # при вызове /stock идут все крафты заново


@asyncio.coroutine
def sync_hero_info():
    yield from client.send_message(charwars_username, "/hero")
    yield from asyncio.sleep(3.0)
    yield from client.send_message(charwars_username, "/stock")
    yield from asyncio.sleep(3.0)


def get_quantity(what: str, where: str):
    result = re.search(what + '.+[^0-9.](\d+)', where)
    if result is None:
        return 0
    else:
        return int(result.group(1))


@asyncio.coroutine
def buy_one_res(res_command):
    yield from client.send_message(charwars_username, res_command)


@asyncio.coroutine
def buy_resources(msg, res):
    for recipe in list(res.values()):
        if recipe["must_craft"]:
            for m in recipe['materials']:
                if (not m['name'] in msg) | (get_quantity(m['name'], msg) < m['amount']):
                    if m['amount'] * m['av_price'] <= local_money:
                        yield from buy_one_res(m['code'])
                        yield from asyncio.sleep(3.0)


@asyncio.coroutine
def process_reply(event):
    global local_money
    msg = event.message.message
    print(time.asctime(), '-', msg)
    f_id = event.message.from_id

    korovans = ["КОРОВАН твой", "КОРОВАН уехал", "пришлось откупиться"]

    if "/go" in msg:
        yield from client.send_message(f_id, "/go")
    if "/engage" in msg:
        yield from client.send_message(f_id, "/engage")
    if any(k in msg for k in korovans):
        yield from sync_hero_info()
        yield from client.send_message(charwars_username, go_korovan_m)
    if "Слишком мало единиц выносливости" in msg:
        yield from sync_hero_info()
    if ("💰" in msg) & ("👝" in msg):
        local_money = int(find_between(msg, '💰', ' 👝'))
        yield from craft_pouch(msg, f_id)
    if "🔋Выносливость:" in msg:
        re_stamina = re.search('Выносливость: (\d+)/(\d+)', msg)
        stamina['now'] = int(re_stamina.group(1))
        stamina['total'] = int(re_stamina.group(2))
    if "📦Склад" in msg:
        yield from buy_resources(msg, resources)  # !!!!!!!!
    if "Получено:" in msg:
        yield from sync_hero_info()


@client.on(events.NewMessage)
@asyncio.coroutine
def my_handler(event: events.NewMessage.Event):
    if event.message.from_id == charwars_id:
        yield from asyncio.sleep(1.0)
        yield from process_reply(event)
        yield from asyncio.sleep(2.0)


@asyncio.coroutine
def async_input(prompt):
    """
    Python's ``input()`` is blocking, which means the event loop we set
    above can't be running while we're blocking there. This method will
    let the loop run while we wait for input.
    """
    print(prompt, end='', flush=True)
    return (yield from loop.run_in_executor(None, sys.stdin.readline)).rstrip()


def exit_check(task, name):
    while True:
        if exit_flg:
            print('closing ' + name + '...')
            return


@asyncio.coroutine
def notify_me(message):
    yield from client.send_message(notify_id, message)


@asyncio.coroutine
def notify_all(message):
    print(message)
    yield from notify_me(message)


@asyncio.coroutine
def test_quit(must_quit, callback):
    while True:
        if must_quit:
            callback()


@asyncio.coroutine
def go_forest(counter, dur_min):
    register_game_task("forestplan")
    print('\n=================')
    yield from notify_all('Идем в ЛЕС#' + str(counter) + ': #' + str(counter + 1) + ' через час')
    yield from client.send_message(charwars_username, go_forest_m)
    yield from asyncio.sleep(7 * 60)
    yield from asyncio.sleep(1.0)
    print("ждем перед следующим походом в лес")
    yield from asyncio.sleep((dur_min - 5) * 60)
    yield from notify_all("ЛЕС#%s #%s через 5 мин" % (str(counter), str(counter + 1)))
    yield from asyncio.sleep(5 * 60)


@asyncio.coroutine
def forest():
    register_game_task("forest")
    try:
        activities['isForest'] = True
        print(datetime.now().strftime("%a %b %d %H:%M:%S") + ': === ежечасные походы в лес ===')
        counter = 0
        while True:
            yield from go_forest(counter, 60)
            counter += 1
    except RuntimeError:
        print('forest cancelled!')
    except CancelledError:
        print('forest cancelled!')


@asyncio.coroutine
def forest_fast():
    register_game_task("fastforest")
    try:
        activities['isFastForest'] = True
        print(datetime.now().strftime("%a %b %d %H:%M:%S") + ': === быстрые походы в лес ===')
        counter = 0
        while True:
            print('\n=================')
            yield from notify_all(
                'Идем в fast ЛЕС#' + str(counter) + ': #' + str(counter + 1) + ' через 7 мин')
            yield from client.send_message(charwars_username, go_forest_m)
            yield from asyncio.sleep(6 * 60)
            yield from notify_all(
                'fast ЛЕС#' + str(counter) + ': #' + str(counter + 1) + 'через 1 мин')
            yield from asyncio.sleep(1 * 60)
            yield from asyncio.sleep(1.0)
            counter += 1
    except RuntimeError:
        print('fast forest cancelled!')
    except CancelledError:
        print('fast forest cancelled!')


def get_korovan_time_remaining():
    time = datetime.today()
    hour = time.hour
    minutes = 60 - time.minute

    if minutes > 9:
        min_str = str(minutes)
    elif minutes == 60:
        hour += 1
        minutes = 0
        min_str = str(minutes)
    else:
        min_str = '0' + str(minutes)
    if hour < k_hour:
        hour_str = str(k_hour - hour - 1)
    else:
        hour_str = str(24 - hour + k_hour - 1)

    time_str = hour_str + ':' + min_str
    message = 'До корованов осталось примерно: ' + time_str
    return message


@asyncio.coroutine
def schedule_korovan():
    register_game_task("korovan")
    try:
        activities['isKorovan'] = True
        yield from notify_all(
            datetime.now().strftime("%a %b %d %H:%M:%S") + ': === ждем наш корован ===')
        while True:
            hour = datetime.today().hour
            if k_hour <= hour < k_hour + 1:
                yield from client.send_message(charwars_username, go_korovan_m)
                yield from notify_all('НАЧИНАЕМ КОРОВАНЫ!')
                activities['isKorovan'] = False
                break
            yield from asyncio.sleep(15 * 60)
            rem_time_message = get_korovan_time_remaining()
            yield from notify_all(rem_time_message)
            yield from asyncio.sleep(1.0)
    except RuntimeError:
        print('korovan cancelled!')
    except CancelledError:
        print('korovan cancelled!')


@asyncio.coroutine
def ka4():
    register_game_task("ka4")
    try:
        activities['isKa4'] = True
        yield from sync_hero_info()
        yield from asyncio.sleep(4)
        f_counter = 0
        should_notify_stamina = True
        yield from notify_all("==Качаемся как не в себя - можно вообще больше комп не трогать==")
        while activity_running('ka4'):
            hours_str = re.search('(\d+):', get_korovan_time_remaining()).group(1)
            hours_before = int(hours_str)
            to_restore = stamina['total'] - stamina['now']
            if (datetime.today().hour >= k_hour + 19) & (not activity_running('korovan')):
                yield from schedule_korovan()
            if (hours_before > to_restore) & (datetime.today().hour >= k_hour + 1):
                yield from go_forest(f_counter, 60)
                msg = "До корованов: {k} ч. \nВыносливости: {s}".format(k=hours_before,
                                                                        s=stamina['now'])
                yield from notify_all(msg)
                f_counter += 1
                should_notify_stamina = True
            elif should_notify_stamina:
                msg = "А теперь бережем выносливость. \nДо корованов: {k} ч. \nВыносливости: {s}" \
                    .format(k=hours_before, s=stamina['now'])
                yield from notify_all(msg)
                yield from asyncio.sleep(5 * 60)
                should_notify_stamina = False
                stop_activity('forest')
    except RuntimeError:
        print('ka4 cancelled')


def shutdown():
    for task in game_tasks:
        if hasattr(task, 'name'):
            print(task.name + ' stopped!')
            task.cancel()
        game_tasks.clear()


def shutdown_or_quit():
    pending = []
    for task in game_tasks:
        if not task.done():
            pending.append(task)
    if len(pending) > 0:
        shutdown()
    else:
        try:
            print('exit')
            sys.exit(-1)
        except RuntimeError:
            print('exit')


def terminate():
    print('exit')
    sys.exit(-1)


@asyncio.coroutine
def run_input_loop():
    print('инициализация ввода команд\n')
    print('вводите команды')
    # while not authorized:
    #     authorized = await client.is_user_authorized()
    #     await asyncio.sleep(1.0)

    while True:
        msg = yield from async_input('')
        # Quit
        if msg == 'q':
            shutdown_or_quit()
        elif msg == 'Q':
            terminate()
            return
        elif msg == 'qq':
            shutdown()
        elif msg == '':
            pass
        elif msg == 'f':
            asyncio.gather(forest())
        elif msg == 'ff':
            asyncio.gather(forest_fast())
        elif msg == 'k':
            asyncio.gather(schedule_korovan())
        elif msg == 'ka4':
            asyncio.gather(ka4())
        elif msg == 'logout':
            print("logged out")
            yield from client.log_out()
            time.sleep(1)
            terminate()
        else:
            yield from client.send_message(charwars_username, msg)


def print_info():
    print('=================')
    print("f - ходить в лес каждый час")
    print("ff - ходить в лес каждые 7 минут")
    print("k - запланировать ограбление всех КОРОВАНОВ в 4 утра (блицкриг)")
    print("q - если есть запланированные действия для бота, завершить их, если нет, то выйти")
    print("ka4 - Ходить в лес каждый час и беречь выносливость для корованов \n"
          "которые каждый день планируются на 4 утра а потом грабятся на всю денюжку")
    print("logout - разлогиниться из аккаутна телеграм")
    print('=================\n')


@asyncio.coroutine
def run_client():
    print_info()
    print('стартуем клиент...')
    yield from client.start()
    print('=================')
    print("BOT STARTED")
    print('=================\n')
    yield from run_input_loop()
    yield from client.run_until_disconnected()


def main():
    tasks = [
        run_client(),
        # run_input_loop()
    ]
    loop.run_until_complete(asyncio.gather(*tasks))


if __name__ == '__main__':

    # get_version_info()

    try:
        main()
    except KeyboardInterrupt:
        print("exited")
        exit_flg = True
    except RuntimeError:
        print('interrupted!')
    finally:
        loop.close()
