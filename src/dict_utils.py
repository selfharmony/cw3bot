import abc

from cffi.backend_ctypes import xrange


class SurrogateDictEntry(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, key):
        """record the key on the real dictionary that this will resolve to a
           value for
        """
        self.key = key

    def resolve(self, d):
        """ return the actual value"""
        if hasattr(self, 'op'):
            # any operation done on self will store it's name in self.op.
            # if this is set, resolve it by calling the appropriate method
            # now that we can get self.value out of d
            self.value = d[self.key]
            return getattr(self, self.op + 'resolve__')()
        else:
            return d[self.key]

    @staticmethod
    def make_op(opname):
        """A convience class. This will be the form of all op hooks for subclasses
           The actual logic for the op is in __op__resolve__ (e.g. __add__resolve__)
        """

        def op(self, other):
            self.stored_value = other
            self.op = opname
            return self

        op.__name__ = opname
        return op


class AdditionSurrogateDictEntry(SurrogateDictEntry):
    __add__ = SurrogateDictEntry.make_op('__add__')
    __radd__ = SurrogateDictEntry.make_op('__radd__')

    def __add__resolve__(self):
        return self.value + self.stored_value

    def __radd__resolve__(self):
        return self.stored_value + self.value


class SurrogateDict(object):
    def __init__(self, EntryClass):
        self.EntryClass = EntryClass

    def __getitem__(self, key):
        """record the key and return"""
        return self.EntryClass(key)

    @staticmethod
    def resolve(d):
        """I eat generators resolve self references"""
        stack = [d]
        while stack:
            cur = stack.pop(0)
            # This just tries to set it to an appropriate iterable
            it = xrange(len(cur)) if not hasattr(cur, 'keys') else cur.keys()
            for key in it:
                # sorry for being a duche. Just register your class with
                # SurrogateDictEntry and you can pass whatever.
                while isinstance(cur[key], SurrogateDictEntry):
                    cur[key] = cur[key].resolve(d)
                # I'm just going to check for iter but you can add other
                # checks here for items that we should loop over.
                if hasattr(cur[key], '__iter__'):
                    stack.append(cur[key])
        return d
