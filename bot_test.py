import importlib
import random
import asyncio, re, socks, sys, multiprocessing, time
from asyncio import CancelledError

from collections import defaultdict
from datetime import datetime
from os import environ
from telethon import TelegramClient, events
import game_data
from game_data import resources
from apscheduler.schedulers.asyncio import AsyncIOScheduler

# import ngrok_listener

api_id = 309116
api_hash = '3e0ef95df207dde3ab60b722d198502a'

# A list of dates of reactions we've sent, so we can keep track of floods
recent_reacts = defaultdict(list)

host = 'sr123.spry.fail'
port = 1080
proxy = (socks.SOCKS5, host, port, None, "telegram", "telegram")

# TG_API_ID and TG_API_HASH *must* exist or this won't run!
session_name = environ.get('TG_SESSION', 'session')
client = TelegramClient(
    session_name, api_id, api_hash,
    proxy=None
)

# app constants
loop = asyncio.get_event_loop()
exit_flg = False
game_tasks = []

# game constants
charwars_username = '@ChatWarsBot'
charwars_id = 265204902
stat_username = "@CWCastleBot"
fv_id = 1001250482144

notify_id = '@SuperBoltunBot'
k_hour = 4
activities = {'isForest': False, 'isKorovan': False, 'isFastForest': False, 'isKa4': False}
stamina = {'now': 5, 'total': 5}
cycles = {'forest': 100}

local_time = {'h': 0, 'm': 0}
local_money = 0
go_korovan_m = "🗡ГРАБИТЬ КОРОВАНЫ"
get_time_m = "/time"
go_forest_m = ["🌲Лес"]
time_before_boss = 999
event_data = {"items": ["Mystery Obsidian", "Shadow Bloodstone", "Timeless Jade", "Void Emerald"], "current": ""}
battles = [0, 8, 16]
scheduler = AsyncIOScheduler()
is_battle = False

my_castle = '🍆'
enemy_castle = '🌹'
pin_insider_cache = []
enemy_is_pinned = False
lil_pin_id = 1293422180
lil_pin_name = "ChatWarslivpin"
pins_dict = {
    '🍆': ['🍆'],
    '🦇': ['🦇'],
    '☘️': ['☘️', '☘'],
    '🍁': ['🍁'],
    '🌹': ['🌹'],
    '🖤': ['🖤'],
    '🐢': ['🐢']
}
protec_m = "🛡Защита"
atac_m = "⚔Атака"


def get_pin(message):
    global enemy_castle
    if enemy_castle in message:
        return enemy_castle
    for key in pins_dict:
        if my_castle in key:
            continue
        for phrase in pins_dict[key]:
            if phrase.lower() in message.lower():
                return key
    return None


# ///////////////////////main////////////////////////////

def roll_quest():
    global go_forest_m
    return random.choice(go_forest_m)
    # return go_forest_m[1]


def register_game_task(name):
    task = asyncio.Task.current_task()
    game_tasks.append(task)
    task.name = name


def stop_activity(name):
    for task in game_tasks:
        if task.name == name:
            task.cancel()
            print(name + " stopped by stopper")


def activity_running(name):
    for task in game_tasks:
        if task.name == name:
            if not task.done():
                return True
    return False


def find_between(string, start, end):
    result = re.search(start + '(\d*)' + end, string)
    return result.group(1)


# флаг накопления необходимой суммы для крафта того или иного ресурса
@asyncio.coroutine
def craft_items(msg, f_id):
    global local_money
    # importlib.reload(game_data)
    proceed = False
    for key, value in resources.items():
        if (resources[key]["must_craft"]) & (resources[key]["to_craft"] > 0):
            enough_money = local_money >= resources[key]["sum"]
            if enough_money:
                yield from asyncio.sleep(3.0)
                yield from client.send_message(f_id, resources[key]["craft"])
                local_money -= resources[key]["sum"]
                resources[key]["to_craft"] -= 1
                yield from asyncio.sleep(3.0)
                proceed = (enough_money | proceed)
    if proceed:
        yield from craft_items(msg, f_id)


@asyncio.coroutine
def get_hero_info():
    yield from client.send_message(charwars_username, "/stock")
    yield from asyncio.sleep(3.0)
    yield from client.send_message(charwars_username, "/hero")
    yield from asyncio.sleep(3.0)
    # yield from client.send_message(charwars_username, "/myshop_open")
    # yield from asyncio.sleep(3.0)


def sync_local_time(msg):
    result = re.search('(\d+):(\d+)', msg)
    local_time['h'] = int(result.group(1))
    local_time['m'] = int(result.group(2))
    asyncio.ensure_future(run_local_time_watch)


@asyncio.coroutine
def run_local_time_watch():
    try:
        register_game_task("time")
        while True:
            yield from asyncio.sleep(15 * 60)
    except RuntimeError:
        print("time sync cancelled")


def get_quantity(what: str, where: str):
    result = re.search(what + '.+[^0-9.](\d+)', where)
    if result is None:
        return 0
    else:
        return int(result.group(1))


@asyncio.coroutine
def buy_one_res(res_command):
    yield from client.send_message(charwars_username, res_command)


@asyncio.coroutine
def buy_resources(msg, res):
    global local_money
    importlib.reload(game_data)
    for recipe in list(res.values()):
        if recipe["must_craft"]:
            if recipe['materials'] is not None:
                materials = random.shuffle(recipe['materials'])
                for m in materials:
                    if (not m['name'] in msg) | (get_quantity(m['name'], msg) < m['amount']):
                        if m['av_price'] <= local_money:
                            yield from asyncio.sleep(3.0)
                            yield from buy_one_res(m['code'])
                            local_money -= m['av_price']
                            yield from asyncio.sleep(3.0)
                            yield from buy_resources(msg, res)


def sync_boss_time_rem(msg):
    global time_before_boss
    result = re.search('⏰(\d+)', msg)
    time_before_boss = int(result.group(1))


def check_time_before_boss(other_task_period_min):
    global time_before_boss
    if time_before_boss < int(other_task_period_min):
        return True
    return False


@asyncio.coroutine
def protec_from_boss():
    global time_before_boss
    time_before_boss = 999
    yield from client.send_message(charwars_username, "🛡Защита")


@asyncio.coroutine
def process_reply(event):
    global local_money, event_data
    msg = event.message.message
    print(time.asctime(), '-', msg)
    f_id = event.message.from_id

    korovans = ["КОРОВАН твой", "КОРОВАН уехал", "пришлось откупиться"]

    if "/go" in msg:
        yield from client.send_message(f_id, "/go")
    if "/engage" in msg:
        yield from client.send_message(f_id, "/engage")
    if any(k in msg for k in korovans):
        yield from asyncio.sleep(1)
        yield from get_hero_info()
        yield from asyncio.sleep(5)
        yield from client.send_message(charwars_username, go_korovan_m)
    if "Слишком мало единиц выносливости" in msg:
        yield from get_hero_info()
    if ("💰" in msg) & ("👝" in msg):
        local_money = int(find_between(msg, '💰', ' 👝'))
        yield from craft_items(msg, f_id)
    if "🔋Выносливость:" in msg:
        re_stamina = re.search('Выносливость: (\d+)/(\d+)', msg)
        stamina['now'] = int(re_stamina.group(1))
        stamina['total'] = int(re_stamina.group(2))
    if "📦Склад" in msg:
        yield from buy_resources(msg, resources)  # !!!!!!!!
    if "Получено:" in msg:
        yield from client.forward_messages(stat_username, event.message)
        yield from get_hero_info()
    if "Вернешься" in msg:
        yield from client.forward_messages(stat_username, event.message)
    if "В мире Chat Wars" in msg:
        sync_local_time(msg)
        yield from client.delete_messages(f_id, event.message.id)
    if "Твои результаты в бою:" in msg:
        yield from client.forward_messages(stat_username, event.message)
    for i in event_data["items"]:
        if i in msg:
            event_data["current"] = i


@client.on(events.NewMessage)
@asyncio.coroutine
def my_handler(event: events.NewMessage.Event):
    global fv_id, enemy_castle, enemy_is_pinned
    if event.message.from_id == charwars_id:
        yield from asyncio.sleep(1.0)
        yield from process_reply(event)
        yield from asyncio.sleep(2.0)
    elif event.message.to_id.channel_id == lil_pin_id:
        pin = get_pin(event.message.message)
        if pin is not None:
            yield from notify_all("message from lilpin: " + pin)
            pin_insider_cache.append(pin)


@asyncio.coroutine
def async_input(prompt):
    """
    Python's ``input()`` is blocking, which means the event loop we set
    above can't be running while we're blocking there. This method will
    let the loop run while we wait for input.
    """
    print(prompt, end='', flush=True)
    return (yield from loop.run_in_executor(None, sys.stdin.readline)).rstrip()


def exit_check(task, name):
    while True:
        if exit_flg:
            print('closing ' + name + '...')
            return


@asyncio.coroutine
def notify_me(message):
    yield from client.send_message(notify_id, message)


@asyncio.coroutine
def notify_all(message):
    print(message)
    yield from notify_me(message)


@asyncio.coroutine
def test_quit(must_quit, callback):
    while True:
        if must_quit:
            callback()


@asyncio.coroutine
def check_time_before_battle():
    global is_battle
    if datetime.today().hour in battles:
        if datetime.today().minute > 40:
            is_battle = True
            yield from asyncio.sleep(50 * 60)
            is_battle = False


def stop_tasks(*tasks):
    for t in tasks:
        if scheduler.get_job(t) is not None:
            scheduler.remove_job(t)


@asyncio.coroutine
def defend():
    global protec_m
    stop_tasks("forest", "f_forest")
    scheduler.add_job(forest, 'interval', minutes=60, id="forest")
    yield from client.send_message(charwars_username, protec_m)


@asyncio.coroutine
def atak():
    yield from notify_all(str(pin_insider_cache))
    if len(pin_insider_cache) > 0:
        yield from client.send_message(charwars_username, pin_insider_cache[-1])
    pin_insider_cache.clear()


@asyncio.coroutine
def prepare_atak():
    global atac_m
    yield from client.send_message(charwars_username, atac_m)


@asyncio.coroutine
def report():
    yield from client.send_message(charwars_username, "/report")


def plan_battles_and_reports():
    global enemy_castle
    scheduler.add_job(defend, 'cron', hour=0, minute=58)
    scheduler.add_job(defend, 'cron', hour=8, minute=58)
    scheduler.add_job(defend, 'cron', hour=16, minute=58)

    scheduler.add_job(atak, 'cron', hour=0, minute=59, second=40)
    scheduler.add_job(atak, 'cron', hour=8, minute=59, second=40)
    scheduler.add_job(atak, 'cron', hour=16, minute=59, second=40)

    scheduler.add_job(prepare_atak, 'cron', hour=0, minute=59, second=20)
    scheduler.add_job(prepare_atak, 'cron', hour=8, minute=59, second=20)
    scheduler.add_job(prepare_atak, 'cron', hour=16, minute=59, second=20)

    scheduler.add_job(report, 'cron', hour=1, minute=11)
    scheduler.add_job(report, 'cron', hour=9, minute=11)
    scheduler.add_job(report, 'cron', hour=17, minute=11)


def get_korovan_time_remaining():
    time = datetime.today()
    hour = time.hour
    minutes = 60 - time.minute

    if minutes > 9:
        min_str = str(minutes)
    elif minutes == 60:
        hour += 1
        minutes = 0
        min_str = str(minutes)
    else:
        min_str = '0' + str(minutes)
    if hour < k_hour:
        hour_str = str(k_hour - hour - 1)
    else:
        hour_str = str(24 - hour + k_hour - 1)

    time_str = hour_str + ':' + min_str
    message = 'До корованов осталось примерно: ' + time_str
    return message


any_forest_counter = 0


@asyncio.coroutine
def forest():
    global any_forest_counter
    yield from notify_all('Идем в ЛЕС#' + str(any_forest_counter))
    yield from client.send_message(charwars_username, roll_quest())
    any_forest_counter += 1


@asyncio.coroutine
def korovan():
    global go_korovan_m
    yield from notify_all('FUCKING KOROVANS')
    yield from client.send_message(charwars_username, go_korovan_m)
    scheduler.add_job(forest, 'interval', minutes=60, id="forest")


@asyncio.coroutine
def check_stamina():
    global stamina, k_hour
    hours_str = re.search('(\d+):', get_korovan_time_remaining()).group(1)
    hours_before = int(hours_str)
    to_restore = stamina['total'] - stamina['now']
    if (hours_before > to_restore) & (datetime.today().hour >= k_hour + 1):
        msg = "До корованов: {k} ч. \nВыносливости: {s}".format(k=hours_before, s=stamina['now'])
        yield from notify_all(msg)
    else:
        msg = "А теперь бережем выносливость. \nДо корованов: {k} ч. \nВыносливости: {s}".format(
            k=hours_before,
            s=stamina['now'])
        stop_tasks("forest", "f_forest")
        yield from notify_all(msg)


def schedule_forest():
    global any_forest_counter
    any_forest_counter = 0
    stop_tasks("forest", "f_forest")
    scheduler.add_job(forest, 'interval', minutes=60, id="forest")
    scheduler.get_job("forest").modify(next_run_time=datetime.now())
    print('=================')
    print('FOREST STARTED!')


def schedule_f_forest():
    global any_forest_counter
    any_forest_counter = 0
    stop_tasks("forest", "f_forest")
    scheduler.add_job(forest, 'interval', minutes=8, id="f_forest")
    scheduler.get_job("f_forest").modify(next_run_time=datetime.now())
    print('=================')
    print('FAST FOREST STARTED!')


def schedule_korovan():
    stop_tasks("korovan")
    scheduler.add_job(korovan, 'cron', hour=4, id="korovan")
    print('=================')
    print('KOROVAN SCHEDULED!')


def schedule_ka4():
    stop_tasks("forest", "f_forest", "korovan")
    schedule_forest()
    schedule_korovan()
    scheduler.add_job(check_stamina, 'interval', minutes=45, id="c_stamina")
    print('=================')
    print('KA4 STARTED!')


@asyncio.coroutine
def plan_auction(lot):
    try:
        register_game_task("auction")
        if not activity_running("time"):
            pass
            # todo отслеживание покупателя, синхронизация имени героя, лимит денег на покупку лота, покупка в последний момент, внутриигровое время в скрипте
    except CancelledError:
        print("auction planning cancelled")
    except RuntimeError:
        print("auction planning cancelled")


def shutdown():
    for task in game_tasks:
        if hasattr(task, 'name'):
            print(task.name + ' stopped!')
            task.cancel()
        game_tasks.clear()


def shutdown_or_quit():
    pending = []
    for task in game_tasks:
        if not task.done():
            pending.append(task)
    if len(pending) > 0:
        shutdown()
    else:
        try:
            print('exit')
            sys.exit(-1)
        except RuntimeError:
            print('exit')


def terminate():
    print('exit')
    sys.exit(-1)


@asyncio.coroutine
def pin_castle(pin: str):
    pin_insider_cache.append(pin)
    yield from notify_all("Добавлен пин на: " + pin)
    yield from notify_all("Полученные пины до битвы: " + str(pin_insider_cache))
    scheduler.add_job(prepare_atak)
    yield from asyncio.sleep(2)
    scheduler.add_job(atak)


@asyncio.coroutine
def run_input_loop():
    print('инициализация ввода команд\n')
    print('вводите команды')
    # while not authorized:
    #     authorized = await client.is_user_authorized()
    #     await asyncio.sleep(1.0)

    while True:
        msg = yield from async_input('')
        # Quit
        if msg == 'q':
            scheduler.remove_all_jobs()
            shutdown_or_quit()
        elif msg == 'Q':
            terminate()
            return
        elif msg == 'qq':
            shutdown()
        elif msg == '':
            pass
        elif msg == 'f':
            schedule_forest()
        elif msg == 'ff':
            schedule_f_forest()
        elif msg == 'k':
            schedule_korovan()
        elif msg == 'ka4':
            schedule_ka4()
        elif msg == 'logout':
            print("logged out")
            yield from client.log_out()
            time.sleep(1)
            terminate()
        elif get_pin(msg) is not None:
            pin = get_pin(msg)
            scheduler.add_job(pin_castle, args=[pin])
        else:
            yield from client.send_message(charwars_username, msg)


def print_info():
    print('=================')
    print("f - ходить в лес каждый час")
    print("ff - ходить в лес каждые 7 минут")
    print("k - запланировать ограбление всех КОРОВАНОВ в 4 утра (блицкриг)")
    print("q - выйти")
    print("ka4 - Ходить в лес каждый час и беречь выносливость для корованов \n"
          "которые каждый день планируются на 4 утра а потом грабятся на всю денюжку")
    print("{castle icon} - добавить пин на иконку замка")
    print('\n--------временно не работают--------')
    print("logout - разлогиниться из аккаутна телеграм")
    print("update - обновить скрипт")
    print("check - проверить версию скрипта")
    print('=================\n')


@asyncio.coroutine
def run_client():
    print_info()
    print('стартуем клиент...')
    yield from client.start()
    print('=================')
    print("BOT STARTED")
    print('=================\n')
    yield from run_input_loop()
    yield from client.run_until_disconnected()


def main():
    tasks = [
        run_client(),
        # run_input_loop()
    ]
    scheduler.start()
    plan_battles_and_reports()
    loop.run_until_complete(asyncio.gather(*tasks))


if __name__ == '__main__':

    # get_version_info()

    try:
        main()
    except KeyboardInterrupt:
        print("exited")
        exit_flg = True
    except RuntimeError:
        print('interrupted!')
    finally:
        loop.close()
