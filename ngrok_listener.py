import asyncio
import re

from telethon import events
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove

file_location = "/Users/evgenyevteev/AndroidStudioProjects/chat-signalr/app/src/main/java/self/harmony/power/chatrtest/data/network/NetworkConfig.kt"
infile_regex_str = 'val NGROK = "(.+?(?="))'


def replace(file_path, pattern, subst, group):
    f = open(file_path, 'r')
    content = f.read()
    match = re.search(pattern, content)
    if group is not None:
        search_result = match.group(group)
    else:
        search_result = match
    f.close()
    content_new = re.sub(search_result, subst, content)
    f = open(file_path, 'w')
    f.write(content_new)
    f.close()


def check_ngrok(event: events.NewMessage.Event):
    if event.message.from_id == 172556307:
        msg = event.message.message
        if ".ngrok.io" in msg:
            match = re.search('//(.+?(?=.ngrok.io))', msg)
            new_str = match.group(1)
            replace(file_location, infile_regex_str, new_str, 1)
            print("ngrok поменялся на: ", new_str)