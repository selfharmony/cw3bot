import asyncio
import logging
import re
import time
import sys
import os

from asyncio import CancelledError

import background_task
from bot_test import get_korovan_time_remaining

from asyncio.streams import StreamWriter, FlowControlMixin

reader, writer = None, None

inter = False


@asyncio.coroutine
def compute(x, y):
    print("Compute %s + %s ..." % (x, y))
    yield from asyncio.sleep(1.0)
    return x + y


@asyncio.coroutine
def print_sum(x, y):
    result = yield from compute(x, y)
    print("%s + %s = %s" % (x, y, result))


@asyncio.coroutine
def ka4():
    global inter
    try:
        while inter is False:
            print("task 1")
            yield from asyncio.sleep(1.3)
            if inter:
                print("Gotta go")
                break
    except RuntimeError:
        print('task 1 RuntimeError!')
    except CancelledError:
        print('task 1 cancelled!')


@asyncio.coroutine
def interrupt():
    global inter
    asyncio.Task.current_task().name = "TASK2"
    try:
        while True:
            yield from asyncio.sleep(1.0)
            print("task 2")
    except RuntimeError:
        print('task 2 RuntimeError!')
    except CancelledError:
        print('task 2 cancelled!')


loop = asyncio.get_event_loop()


def shutdown():
    for task in asyncio.Task.all_tasks():
        if task is not asyncio.tasks.Task.current_task():
            task.cancel()


@asyncio.coroutine
def stdio(loop=None):
    if loop is None:
        loop = asyncio.get_event_loop()

    reader = asyncio.StreamReader()
    reader_protocol = asyncio.StreamReaderProtocol(reader)

    writer_transport, writer_protocol = yield from loop.connect_write_pipe(FlowControlMixin,
                                                                           os.fdopen(1, 'wb'))
    writer = StreamWriter(writer_transport, writer_protocol, None, loop)

    yield from loop.connect_read_pipe(lambda: reader_protocol, sys.stdin)

    return reader, writer


@asyncio.coroutine
def async_input(message):
    if isinstance(message, str):
        message = message.encode('utf8')

    global reader, writer
    if (reader, writer) == (None, None):
        reader, writer = yield from stdio()

    writer.write(message)
    yield from writer.drain()

    line = yield from reader.readline()
    return line.decode('utf8').replace('\r', '').replace('\n', '')


@asyncio.coroutine
def input_loop():
    while True:
        command = yield from async_input("")
        if command == 'q':
            shutdown_or_quit()


def shutdown_or_quit():
    pending = []
    for task in asyncio.Task.all_tasks():
        if not task.done():
            pending.append(task)
            if hasattr(task, 'name'):
                print(task.name)
    if len(pending) > 1:
        shutdown()
    else:
        loop.close()


@asyncio.coroutine
def test_ka4():
    notify_stamina_low = True
    i = 0
    while True:
        if i < 20:
            if i > 10:
                print('korovan ', i - 10)
                yield from asyncio.sleep(0.4)
        if i < 10:
            print('forest ', i)
            yield from asyncio.sleep(0.4)
        elif i == 10:
            print("А теперь бережем выносливость")
            yield from asyncio.sleep(0.4)
        i += 1
        if i >= 20:
            i = 0


tasks = [
    # ka4(),
    # interrupt(),
    # input_loop(),
    test_ka4()
]

# finished, pending = loop.run_until_complete(
#     asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED))
# print(">> Finished: {}", finished)
# print(">> Pending: {}", pending)

# loop.run_until_complete(print_sum(1, 2))


try:
    loop.run_until_complete(asyncio.gather(*tasks))
except RuntimeError:
    print('interrupted!')
    loop.close()
except KeyboardInterrupt:
    print('interrupted!')
    loop.close()

# hours_before = 4
# stamina=3
# msg = "А теперь бережем выносливость. \nДо корованов: {k} час/а \nВыносливости: {s}" \
#     .format(k=hours_before, s=stamina)
# print(msg)


# msg = re.search('(\d+):', get_korovan_time_remaining()).group(1)
# print(msg)
